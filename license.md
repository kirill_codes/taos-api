
Copyright (C) Kirill Chernik (kirill)
All Rights Reserved.

Unauthorized duplication, modification or usage
of any files or materials protected by this license,
via any medium is strictly prohibited.

Proprietary and confidential.
