

# TAOS Api

## REST API:

### Auth:

`/auth/login`

POST REQUEST
{ email: string; secret: string; }

RESPONSE
{ id: string; token: string;}

`/auth/signup`

POST REQUEST
{
  email: string;
  secret: string;
  type?: "BUSINESS" | "INDIVIDUAL" | "ACCOUNTANT";
  name_company?: string;
}

RESPONSE
{
  id: string;
  token: string;
}

Create Client application for Accountants to review:

`/create`

POST REQUEST
{
  income_tax_types: Array<string>
  state_residence: string;
  state_income_source?: string;
}

RESPONSE
{ id: string; }

## GQL API:
Fetches information from the database.
Requires Authorization Header
Schema defined in `/dao`
