
create database taos;
create extension pgcrypto;

create type type_of_account as enum (
  'INDIVIDUAL', 'BUSINESS', 'ACCOUNTANT'
);

create type type_of_income_tax as enum (
  'INCOME_W2', 'INCOME_1040', 'INCOME_1099',
  'INCOME_1099B', 'INCOME_1099INT',
  'INCOME_1040_SR', 'SELF_EMPLOYED', 'FUTA'
);

create type type_of_document as enum (
  'TAX_CHILD', 'EITC', 'DONATION', 'OTHER'
);

create type status_of_document as enum (
  'REQUIRED', 'UPLOADED', 'PENDING_REVIEW', 'REJECTED', 'APPROVED'
);

create table clients (
  id uuid not null default gen_random_uuid(),
  "type" type_of_account not null default 'INDIVIDUAL',
  form_id uuid,
  name_given text,
  name_last text,
  name_business text,
  email text,
  secret text,
  assigned_company_id uuid
);

alter table clients add constraint uniq_email unique (email);

create table accountants (
  id uuid not null default gen_random_uuid(),
  "type" type_of_account not null default 'ACCOUNTANT',
  email text,
  secret text,
  name_company text not null
);

alter table accountants add constraint uniq_email_accountants unique (email);

create table documents (
  id uuid not null default gen_random_uuid(),
  user_id uuid not null,
  "type" type_of_document not null,
  status status_of_document not null
);

create table forms (
  id uuid not null default gen_random_uuid(),
  user_id uuid not null,
  accountant_id uuid,

  income_tax_types type_of_income_tax[],
  state_residence text,
  state_income_source text,

  additional_commentary text
);
