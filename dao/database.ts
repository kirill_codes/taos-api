/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-implicit-any-catch */

import { Pool } from "pg";

const {
  PG_CONN_WRITER = "postgres://localhost/taos",
  PG_CONN_READER = "postgres://localhost/taos",
} = process.env;

export const writer = new Pool({ connectionString: PG_CONN_WRITER, });

export type Query = [ string ] | [ string, Array<unknown> | undefined ];

export async function writeQuery ( query: string, values?: Array<unknown> ) {
  const client = await writer.connect();

  try {
    await client.query("BEGIN");
    await client.query(query, values);

    return await client.query("COMMIT");
  } catch ( e: unknown ) {
    await client.query("ROLLBACK");
    throw e;
  } finally {
    client.release();
  }
}

export const reader = new Pool({ connectionString: PG_CONN_READER, });

export async function readQuery ( query: string, values?: Array<unknown> ) {
  const client = await reader.connect();

  try {
    return await client.query(query, values);
  } catch ( e: unknown ) {
    await client.query("ROLLBACK");
    throw e;
  } finally {
    client.release();
  }
}
