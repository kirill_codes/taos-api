
import { verify } from "./jwt";
import { writeQuery } from "../dao/database";
import type { Request, Response } from "express";

interface ApplicationBody {
  income_tax_types: Array<string>;
  state_residence: string;
  state_income_source?: string;
}

export async function createApplication ( req: Request, res: Response ) {
  const token = req.headers?.authorization?.split(" ")?.[1];
  const claims = token && verify("ok", token);

  const result = await writeQuery(
    `insert into forms (
      user_id, income_tax_types, state_residence, state_income_source
    ) values( $1, $2, $3, $4) returning id`, [
      (<{ id: string; }>claims)?.id,
      (<ApplicationBody>req.body)?.income_tax_types,
      (<ApplicationBody>req.body)?.state_residence,
      (<ApplicationBody>req.body)?.state_income_source,
    ]
  );

  if (result.rowCount) {
    const [ { id, }, ] = result.rows;

    return res.json({ id, });
  }

  return res.send("Internal Error");
}
