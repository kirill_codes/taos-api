/* eslint-disable @typescript-eslint/no-implicit-any-catch */
/* eslint-disable max-lines-per-function */

interface AuthBody {
  email: string;
  secret: string;
  name_company?: string;
  type: "INDIVIDUAL" | "BUSINESS" | "ACCOUNTANT";
}

import { sign } from "./jwt";
import type { Request, Response } from "express";
import { readQuery, writeQuery } from "../dao/database";

export async function login ( req: Request, res: Response ) {
  const isClient = "ACCOUNTANT" !== (<AuthBody>req.body)?.type;

  const result = await readQuery(`
    select id, secret = crypt($2, secret) as success, "type"
      from ${isClient ? "clients" : "accountants"} where email = $1`, [
    (<AuthBody>req.body)?.email,
    (<AuthBody>req.body)?.secret,
  ]);

  if (result.rowCount && result.rows[0].success) {
    const [ { id, success, type, }, ] = result.rows;

    if (!id) { return res.send("Account not found"); }

    if (!success) { return res.send("Incorrect secret"); }

    return res.json({ id, token: sign("ok", { id, role: type, }), });
  }

  return res.send("Internal Error");
}

export async function signup ( req: Request, res: Response ) {
  const type = (<AuthBody>req.body)?.type;

  const isClient = "ACCOUNTANT" !== type;

  try {
    const result = await writeQuery(`${isClient
      ? "insert into clients (email, secret, \"type\") "
      : "insert into accountants (email, secret, name_company) "}
        values(
          $1, crypt($2, gen_salt('bf', 8)),
          ${isClient ? "coalesce($3, 'INDIVIDUAL'::type_of_account)" : "$3"}
      ) returning id`, [
      (<AuthBody>req.body)?.email,
      (<AuthBody>req.body)?.secret,
      isClient ? type : (<AuthBody>req.body).name_company!,
    ]);

    if (result.rowCount) {
      const [ { id, }, ] = result.rows;

      return res.json({
        id,
        token: sign("ok", { id, role: type || "CLIENT", }),
      });
    }

    return res.send("Something went wrong");
  } catch ( error ) {
    return ("uniq_email" === error?.constraint || "uniq_email_accountant" === error?.constraint)
      ? res.send("Conflict") : res.send(error.message || error);
  }
}
