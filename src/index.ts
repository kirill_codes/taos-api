/* eslint-disable @typescript-eslint/no-misused-promises */
/* eslint-disable no-console */
/* eslint-disable max-lines-per-function */

import { ApolloServer } from "@apollo/server";
import { ApolloServerPluginDrainHttpServer } from "@apollo/server/plugin/drainHttpServer";
import { createApplication } from "./application";
import express from "express";
import { expressMiddleware } from "@apollo/server/express4";
import format from "pg-format";
import { getSchema } from "./schema";
import http from "http";
import { json } from "body-parser";
import { readQuery } from "../dao/database";
import { verify } from "./jwt";
import { login, signup } from "./auth";

(async function main ( ) {
  const app = express();
  const httpServer = http.createServer(app);

  const apollo = new ApolloServer({
    plugins: [ ApolloServerPluginDrainHttpServer({ httpServer, }), ],
    schema : getSchema(),
  });

  await apollo.start();

  app.post("/auth/login", login);
  app.post("/auth/signup", signup);
  app.post("/create", createApplication); // aka form

  app.use("/", json());
  app.use(
    "/graphql",
    expressMiddleware(apollo, {
      async context ( { req, res, } ) {
        const token = req.headers?.authorization?.split(" ")?.[1];

        if (!token) { res.send("Unauthorized"); }

        try {
          const claims = token && verify("ok", token);

          return {
            claims,
            format (
              clientString: string,
              accountantString: string,
              args: string
            ) {
              return "ACCOUNTANT" !== (<{ role: string; }>claims)?.role
                ? format(clientString, args) : format(accountantString, args);
            },
            query: readQuery,
          };
        } catch ( e: unknown ) { return res.send( e || "Internal Error"); }
      },
    })
  );

  httpServer.listen({ port: 4000, });

  console.log("listening on http://localhost:4000/graphql");
})().catch(console.error.bind(console));
