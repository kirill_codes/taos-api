
import { readFileSync } from "fs";
import { withJm } from "@cran/gql.jm";
import { composeExecutableSchema, withCoreFeatures } from "@cran/gql.core";

function readSdl ( name: string ) {
  return readFileSync(
    `${__dirname}/../../dao/${name}`, "utf-8"
  );
}

export function getSchema ( ) {
  return composeExecutableSchema({
    typeDefs: [
      readSdl("0-enums.gql"),
      readSdl("1-main.gql"),
    ].filter(Boolean),
    plugins: [
      ...withCoreFeatures({ }),
      ...withJm({ resolverOptions: { dialect: "pg", }, }),
    ],
  });
}
