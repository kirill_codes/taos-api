
import type { JwtPayload } from "jsonwebtoken";
import { sign as jwtSign, verify as jwtVerify } from "jsonwebtoken";

const { JWT_SECRET = "secret", } = process.env;

export function sign ( subject: string, payload: object ) {
  return jwtSign(payload, JWT_SECRET, {
    algorithm: "HS256", subject,
  });
}

export function verify ( subject: string, token: string ) {
  return <JwtPayload> jwtVerify(token, JWT_SECRET, {
    subject, complete: false,
  });
}
